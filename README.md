# **Tecnológico Nacional de México**
**Instituto Tecnológico de Oaxaca**
## Programación Lógica y Funcional
SCC1019 ISB
### Act 3. Matemáticas

#### _**Mapa 1: Conjuntos, Aplicaciones y funciones**_
Año de autoria: 2002

```plantuml
@startmindmap
title Mapa 1: Conjuntos, Aplicaciones y funciones.
*[#Coral] Conjuntos,\nAplicaciones y\nfunciones

**[#Red] Conceptos generales
***_ es
****[#Cyan] pensamiento matemático
*****_ las
******[#Cyan] entidades matemáticas
*******_ capacidad
********[#Cyan] Capaz de encontrar\npropiedades validas
*********_ para no
**********[#Cyan] demostrarlas caso por caso
*****_ es cercano a
******[#Cyan] Filosofia
*****_ que incorpora
******[#Cyan] conceptos mas allá\nde números
*******_ esto es
********[#Cyan] Conjuntos
*********_ conforman
**********[#Lime] Transformaciones
**********[#Lime] Operaciones
**********[#Lime] Relaciones

**[#Red] Conjunto
***_ tipos
****[#Cyan] Universal
*****_ es
******[#Cyan] conjunto de referencia en el que\ocurren todas las cosas de la teoria
****[#Cyan] Vacio
*****_ es la
******[#Cyan] necesidad logica
*******_ que
********[#Lime] cierra bien las cosas
********[#Lime] conforma un conjunto que\nno tiene elemento

***_ es
****[#Cyan] afán matemático
*****_ que busca
******[#Cyan] la razón última\nde las cosas
*******_ bajo esa premisa
********[#Cyan] Se encuentran

*********[#lime] conjunto
**********_ son
***********[#Cyan] conceptos que no\nse definen
************_ porque
*************[#Cyan] culaquier definición seria "similar"

*********[#Lime] elemento
**********_ es
***********[#Cyan] idea intuitiva asumida\npor el observador

*******_ no se define
********[#Cyan] relación entre\nconjunto y elemento
*********_ llamada tambien
**********[#Cyan] Relación de pertenencia
***********_ definición
************[#Cyan] Dado un elemento y un conjunto\nse puede saber
*************_ si
**************[#Cyan] el elemento está dentro\no no del conjunto
***********_ teoriza
************[#Cyan] de forma abstracta
*************_ que conforma
**************[#Cyan] Teoria del Conjunto

**********[#Cyan] La inclusión
***********_ es
************[#Cyan] la primera noción que\nhay entre conjuntos
*************_ es
**************[#Cyan] un trasunto de orden de numeros
**************_ de tal manera que
***************[#Lime] Un conjunto está\contenido en otro
***************[#Lime] Elementos del primero conjunto,\npertenecen al segundo

**[#Red] Operaciones de conjuntos
***_ se retoman en
****[#Lime] Lógica
****[#Lime] Porbabilidad
***_ elementos

****[#Lime] Interseccion
*****_ son
******[#Cyan] elementos que pertenecen\nsimultaneamente a ambos
****[#Lime] Union
*****_ son
******[#Cyan] elementos que pertenecen\nal menos a un conjunto
****[#Lime] Complementación
*****_ son
******[#Cyan] aquellos que no pertenecen\na un conjunto dado
***_ cuando
****[#Cyan] realizan varias operaciones
*****_ son
******[#Cyan] Diferencia de Conjuntos

**[#Red] Representaciones matemáticas
***_ realizadas\nmediante
****[#Cyan] Dibujos y graficas
*****_ como

******[#Lime] Diagrama de Venn
*******_ hecho por
********[#Cyan] John Venn
*******_ proposito
********[#Lime] ayudan a entender los conjuntos
********[#Lime] ayudan a entender la posición\nde las operaciones
*********_ de tipo
**********[#Lime] Dos conjuntos
**********[#Lime] Tres conjuntos
**********[#Lime] n conjuntos
*******_ usa
********[#Lime] Coleccion de elementos
********[#Lime] Figuras
*********_ como
**********[#SpringGreen] Circulos
**********[#SpringGreen] Ovalos
**********[#SpringGreen] Lineas cerradas
***********_ que
************[#Cyan] encierran elementos\ndel conjunto
*******_ no es
********[#Cyan] un método de demostración
*********_ para eso
**********[#Lime] usan definiciones de\nmanera exclusiva
**********[#Lime] se recomienda para una ejemplificación

**[#Red] Cardinal de un conjunto
***_ es
****[#Cyan] Numero de elementos que\nconforman el conjunto
***_ une
****[#Cyan] la teoria de los\nnumeros naturales
*****_ con
******[#Cyan] la teoria de conjuntos
***_ contiene
****[#Cyan] formula para resolver\nproblemas de cardinal
*****_ que son
******[#Lime] cardinal de conjuntos
*******_ que dice
********[#Cyan] la cardinal de una union\nes igual a la cardinal de\nuno de los conjuntos
*********_ más
**********[#Cyan] el cardinal del segundo conjunto
***********_ menos
************[#Cyan] el cardinal de la intersección
******[#Lime] acotación de cardinales

**[#Red] Aplicaciones y funciones
***_ en
****[#Cyan] disiplinas cientificas
*****_ se conoce como
******[#Cyan] transformación y cambio
*******[#Cyan] es parte esencial de la humanidad
********_ está presente en
*********[#Lime] gobierno
*********[#Lime] biologia
*********[#Lime] economia
*******[#Cyan] definición
********[#Cyan] Identificar que elemento A\nse convierte con el B
*******[#Cyan] la aplicación
********_ es
*********[#Cyan] clase de transformación
**********_ donde
***********[#Cyan] la transformacion convierte a\ncada uno de los elementos\nde un conjunto en\nun unico elemento del conjunto destino

**[#Red] Imagen e Imagen Inversa
***_ de
****[#Cyan] un conjunto o de un subconjunto
***_ tipos de aplicaciones
****[#Lime] Inyectiva
****[#Lime] Subjetiva
****[#Lime] Biyectiva
***_ definicion
****[#Cyan] son nociones tecnicas
*****_ que hacen
******[#Cyan] realizar varios ejemplos

**[#Red] Transformación de\ntransformaciones
***_ es
****[#Cyan] una idea
***_ conduce a
****[#Cyan] una aplicacion primero y despues\n a otra aplicacion al\mtransformado mediante la\nprimera aplicacion
*****_ esto se llama
******[#Cyan] Composicion de aplicaciones

**[#Red] ¿A que se le denomina funcion?
***[#Lime] cuando haya una transformación o aplicacion
****_ entre
*****[#Cyan] conjunto de numeros
******_ se usa
*******[#Cyan] la palabra funcion
********_ se representa
*********[#Cyan] grafica de la funcion
**********_ con representaciones como
***********[#Lime] lineas rectas
***********[#Lime] parabolas
***********[#Lime] serie de puntos
***[#Lime] la particularidad
****_ consiste
*****[#Cyan] Los conjuntos que se transforman\nson conjuntos de numeros
******_ como
*******[#SpringGreen] Numeros reales
*******[#SpringGreen] Numeros naturales
*******[#SpringGreen] Numeros enteros
*******[#SpringGreen] Numeros fraccionarios

**[#Red] Vocaciones de la matemática
***_ desarrollar
****[#Cyan] razonamiento
*****_ de tipo
******[#Cyan] abstracto
*****_ finalidad
******[#Cyan] objetivo básico
***_ otorgar
****[#Cyan] solución a problemas

header
Rodriguez Martinez Joel Jafet
endheader

@endmindmap
```

#### _**Mapa 2: Funciones**_
Año de autoria: 2010

```plantuml
@startmindmap
title Mapa 2: Funciones.
*[#Coral] Funciones

**[#Red] Conceptos basicos
***_ considerado
****[#Cyan] corazon de las matematicas
***_ refleja 
****[#Cyan] lo que ha hecho el hombre
*****_ para
******[#Lime] entender su entorno
******[#Lime] resolver problemas cotidianos
***_ mediciones
****[#Cyan] Pueden variar dependiendo\ndel lugar
*****_ por ejemplo
******[#Lime] Temperatura
******[#Lime] Cambio de divisas
***_ Origina
****[#Cyan] por causa del cambio
*****_ objetivo
******[#Lime] Saber que matematicas hay\ndebajo del calculo
******[#Lime] Preveer, conocer e\nintuir el cambio
*****_ asi
******[#Cyan] se desarrollaron ciencias
*******_ como
********[#Lime] Calculo
********[#Lime] geometria
********[#Lime] Distancias
********[#Lime] Logica

**[#Red] Aplicacion especial
***_ donde
****[#Cyan] los conjuntos son de numeros
*****_ pero
******[#Cyan] esta idea tardó mucho\nen asentarse
***_ ejemplo
****[#Cyan] Una funcion que a cada numero\nse le asigna un cuadrado

**[#Red] Caracteristicas 
***[#Cyan] Representacion grafica
****_ es
*****[#Cyan] representacion grafica
******_ en
*******[#Cyan] plano cartesiano
********_ conforman
*********[#Lime] Red horizontal\no eje de las X
*********[#Lime] Eje de los numeros reales\no eje vertical\no eje de las f(x)\no Imagen
********_ donde
*********[#Cyan] los conjuntos de puntos X y Y
**********_ hacen
***********[#Cyan] la grafica de la funcion

***[#Cyan] Tipos
****[#Lime] Decreciente
*****_ que es
******[#Cyan] Un numero que se le asigna su opuesto
*******_ tambien
********[#Cyan] disminuye su imagen
****[#Lime] Creciente
*****_ que es
******[#Cyan] Cuando aumenta la variable\nindependiente, aumenta\nsu imagen

***[#Cyan] Relativos
****_ son
*****[#Lime] Maximo
******_ son
*******[#Cyan] cuando sus valores de la funcion\nsuben, pero llega a un punto\nque empiezan a decrecer
*****[#Lime] Minimo
******_ son
*******[#Cyan] Cuando sus valores de la funcion\nbajan, pero llega a un punto\nque empiezan a crecer
********_ ejemplo
*********[#Cyan] Cambio de clima

**[#Red] Limite
***_ es
****[#Lime] comportamiento local
****[#Lime] Es limite de funcion en un punto
*****_ puede
******[#Cyan] carecer de limite\ncuando tiende a cero
*****_ cuando
******[#Cyan] se consideran para la variable x que\nestan cerca del punto
*******_ y
********[#Cyan] los valores de f(x) estan cerca\nde un determinado valor

**[#Red] Continuidad
***_ definicion
****[#Cyan] Es la funcion que tiene buen comportamiento
*****_ eso significa
******[#Cyan] Funciones más manejables

**[#Red] Derivada
***_ pregunta
****[#Cyan] ¿Cómo aproximar relativamente\nuna funcion complicada\ncon una más simple?
*****_ propiedad
******[#Cyan] son lineales
*****_ sintesis
******[#Cyan] Resolver el problema de la aproximación\n de una funcion compleja
*******_ mediante
********[#Cyan] Una funcion lineal más simple
*********_ ejemplo
**********[#Cyan] Pendiente de una tangente

header
Rodriguez Martinez Joel Jafet
endheader

@endmindmap
```

#### _**Mapa 3: La matemática del computador**_
Año de autoria: 2002

```plantuml
@startmindmap
title Mapa 3: La matemática del computador.
*[#Coral] La matemática\ndel computador

**[#Red] Aritmetica del computador
**_ conforman
***[#Cyan] operaciones binarias
****_ usando
*****[#Cyan] base 2
****_ como
*****[#Lime] sumas
*****[#Lime] restas
**_ hacian
***[#Cyan] Calculos con numeros abstractos en papel
****_ como
*****[#SpringGreen] Numero pi
*****[#SpringGreen] Numeros infinitos

**[#Red] Caracteristicas
***_ para
****[#Cyan] entender los conceptos
*****_ como
******[#Lime] Numero aproximado
******[#Lime] Numero de error
******[#Lime] Digitos significativos

******[#Cyan] Truncar y redondear numeros
*******_ definicion

********[#Cyan] cuando se tiene una gran cantidad de numeros\ny se tiene que recortar para\nlectura
*********_ cuando se usa
**********[#Lime] numeros grandes
**********[#Lime] numeros muy pequeños

********[#SpringGreen] truncar
*********_ es
**********[#Cyan] cortar un numero a partir de numeros a la derecha
***********_ ejemplo
************[#Cyan] el valor real de Pi

*********[#SpringGreen] redondear
**********[#Cyan] Intentar que el error cometido sea enmedado
***********_ considerado como
************[#Cyan] Truncado sofisticado

**[#Red] Conceptos basicos
***[#Cyan] la matematica
****_ ha sido
*****[#Cyan] la base para las computadoras
***_ pregunta
****[#Cyan] ¿El sistema que usa el computador, puede tener alguna dificultad?
*****_ está
******[#Cyan] hecho a base de numeros binarios
*******_ con eso
********[#Cyan] se puede armar\nsistemas complejos
*********_ a traves de eso
**********[#Cyan] se representan
***********[#Lime] imagenes
***********[#Lime] videos
***********[#Lime] multimedia
*******_ contextualiza
********[#Cyan] logica binaria
*********_ como
**********[#Cyan] encendido y apagado

***_ enlaza con
****[#Lime] logica booleana
****[#Lime] logica de conjuntos

***_ Usa
****[#Lime] Notacion cientifica
****[#Lime] Exponencial normalizada
****[#Lime] sistema octal y hexadecimal
*****_ que son
******[#Cyan] representaciones compactas de numeros

***_ Representación
****[#Cyan] Se puede representar un numero mediante bits
*****_ en
******[#Cyan] ese numero finito de posiciones
*******[#Cyan] se puede representar
********_ bajo
*********[#Cyan] ideas matematicas
**********_ como
***********[#Lime] representacion interna de numeros
***********[#Lime] representacion en signos
***********[#Lime] representacion en complemento
***********[#Lime] representacion de punto flotante
***********[#Lime] representacion de desbordamiento


header
Rodriguez Martinez Joel Jafet
endheader

@endmindmap
```
